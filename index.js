var path = require('path');
var fs = require('fs');
var log = require('winston');
var async = require('async');
var extend = require('util')._extend;


exports.package = function(options, cb) {

    options = prepareOptions(options);

    gitVersionDnxConsole(options, function() {
        require('./package.js')(options, cb);
    })
}

exports.configure = {};
exports.configure.get = function(options, cb) {

    log.level = options.log_level;


    require('./configure.js').get(prepareOptions(options), cb);
}

exports.configure.set = function(options, cb) {

    log.level = options.log_level;


    require('./configure.js').set(prepareOptions(options), cb);
}

exports.install = function(options, cb) {

    log.level = options.log_level;

    if (options.env) {
        options.fn.envHelper.write(".env", options.env);
        return cb();
    } else {
        cb();
    }
}

function packageFilepath(options) {
    return path.join(options.outputDir, packageFilename(options, options));
}

function packageFilename(options) {
    return [options.project, '-', options.version, '.zip'].join('');
}


function packageEnvFilepath(options) {
    return path.join(options.outputDir, [options.project, '-', options.version, '.env'].join(''));
}


function readPackageJson() {
    var projectJsonPath = path.resolve('project.json');
    if (!fs.existsSync(projectJsonPath)) throw "Can't find project.json. Looked here: " + projectJsonPath;
    return require(projectJsonPath);
}


function gitVersionDnxConsole(options, cb) {
    options.fn.gitVersion(options.packageJson.version, function(error, v) {

        if (error) return cb(error);

        options.version = v;

        log.info('Git Version Calculated: %s', options.version);

        cb(null, options);
    });
}


function prepareOptions(options) {

    options.packageJson = readPackageJson();
    options.outputDir = options.outputDir || ['..', 'dist', options.project].join(path.sep);

    options.dnxConsoleFn = {
        packageFilepath: packageFilepath,
        packageFilename: packageFilename,
        packageEnvFilepath: packageEnvFilepath
    };

    if (!options.project) {
        options.project = path.basename(process.cwd());

    }

    return options;
}
